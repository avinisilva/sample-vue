import Vue from 'vue'
import VueMaterial from 'vue-material'
import router from './router'
import store from './vuex'
import App from './app/Main.vue'

Vue.use(VueMaterial)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
