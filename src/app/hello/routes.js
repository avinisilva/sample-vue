import Hello from './components/Hello'

export default [
  { path: '/hello/:name', component: Hello }
]
