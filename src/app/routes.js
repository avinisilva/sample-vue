import { routes as dashboard } from './dashboard'
import { routes as hello } from './hello'

export default [ ...dashboard, ...hello ]
